package com.ee.intprep.repositories;

import com.ee.intprep.entity.Label;
import com.ee.intprep.entity.question.Question;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface QuestionRepository extends CrudRepository<Question, Long> {

    List<Question> findAll();

    List<Question> findByTextContaining(String phrase);

    List<Question> findByLabelsIn(List<Label> labels);

    List<Question> findByTextContainingAndLabelsIn(String phrase, List<Label> labels);
}
