package com.ee.intprep.repositories;

import com.ee.intprep.entity.Label;
import org.springframework.data.repository.CrudRepository;

public interface LabelRepository extends CrudRepository<Label, Long> {
}
