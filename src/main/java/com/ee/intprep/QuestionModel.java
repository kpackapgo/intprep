package com.ee.intprep;

import com.ee.intprep.common.QuestionType;
import com.ee.intprep.entity.Label;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
public class QuestionModel {

    private long id;
    private String text;
    private QuestionType questionType;
    private boolean hidden;
    private Set<Label> labels;

    private String singleChoiceExpectedAnswer;
    private List<String> singleChoiceAnswers;

    private List<String> multipleChoiceExpectedAnswers;
    private List<String> multipleChoiceAnswers;
}
