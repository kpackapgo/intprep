package com.ee.intprep.common;

import com.ee.intprep.QuestionModel;
import com.ee.intprep.entity.question.Question;
import com.google.common.collect.Lists;

import java.util.List;

public class QuestionUtils {

    public static List<QuestionModel> convertToModel(List<Question> questions) {
        List<QuestionModel> models = Lists.newArrayList();
        for (Question question : questions) {
            QuestionModel questionModel = new QuestionModel();
            questionModel.setId(question.getId());
            questionModel.setQuestionType(question.getType());
            questionModel.setText(question.getText());

            models.add(questionModel);
        }

        return models;
    }
}
