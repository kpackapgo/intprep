package com.ee.intprep.common;

public enum QuestionType {

    SINGLE_CHOICE, MULTIPLE_CHOICE, OPEN
}
