package com.ee.intprep.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Label {

    @Id
    @GeneratedValue
    private long id;

    private String name;
}
