package com.ee.intprep.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class QuestionBundle {

    @Id
    private long id;
}
