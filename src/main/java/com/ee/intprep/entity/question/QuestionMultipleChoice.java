package com.ee.intprep.entity.question;

import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = QuestionSingleChoice.TABLE_NAME)
@PrimaryKeyJoinColumn(name = "id")
@Getter
@Setter
@NoArgsConstructor
public class QuestionMultipleChoice extends Question {

    public static final String TABLE_NAME = Question.TABLE_NAME + "_multiple_choice";

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name = TABLE_NAME + "_choices")
    @Column(name = "choices")
    private List<String> choices = new ArrayList<>();

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name = TABLE_NAME + "_expected_value")
    @Column(name = "expected_value")
    private List<String> expectedValue = new ArrayList<>();


}
