package com.ee.intprep.entity.question;

import com.ee.intprep.common.QuestionType;
import com.ee.intprep.entity.Label;
import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = Question.TABLE_NAME)
public class Question {

    public static final String TABLE_NAME = "question";

    @Id
    @GeneratedValue
    private long id;

    private String text;

    private QuestionType type;

    @ManyToMany
    private Set<Label> labels = new HashSet<>();

    private boolean hidden;
}
