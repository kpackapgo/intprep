package com.ee.intprep.entity.question;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
@NoArgsConstructor
public class QuestionOpen extends Question {

    public static final String TABLE_NAME = Question.TABLE_NAME + "_open";

    @Column(name = "expected_value")
    private String expectedValue;
}
