package com.ee.intprep.entity.question;

import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = QuestionSingleChoice.TABLE_NAME)
@PrimaryKeyJoinColumn(name = "id")
@Getter
@Setter
@NoArgsConstructor
public class QuestionSingleChoice extends Question{

    public static final String TABLE_NAME = Question.TABLE_NAME + "_single_choice";

    @ElementCollection
    @LazyCollection(LazyCollectionOption.FALSE)
    @CollectionTable(name = TABLE_NAME + "_choices")
    @Column(name = "choices")
    private List<String> choices = new ArrayList<>();

    @Column(name = "expected_value")
    private String expectedValue;

}
