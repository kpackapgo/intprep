package com.ee.intprep.controller;

import com.ee.intprep.QuestionModel;
import com.ee.intprep.SearchFilter;
import com.ee.intprep.common.QuestionType;
import com.ee.intprep.entity.Label;
import com.ee.intprep.entity.question.Question;
import com.ee.intprep.entity.question.QuestionMultipleChoice;
import com.ee.intprep.entity.question.QuestionSingleChoice;
import com.ee.intprep.repositories.LabelRepository;
import com.ee.intprep.services.QuestionService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Set;

@Controller
public class QuestionController {

    private QuestionService questionService;
    private LabelRepository labelRepository;

    @Autowired
    public QuestionController(QuestionService questionService, LabelRepository labelRepository) {
        this.questionService = questionService;
        this.labelRepository = labelRepository;
    }

    @RequestMapping(value = "/createquestion")
    public ModelAndView goToQuestionCreate() {
        ModelAndView modelAndView = new ModelAndView("question_create");

        QuestionModel question = new QuestionModel();
        Set<Label> allLabels = Sets.newHashSet(labelRepository.findAll());
        question.setLabels(allLabels);
        modelAndView.addObject("question", question);

        return modelAndView;
    }

    @PostMapping("/createupdatequestion")
    public String createOrUpdateQuestion(QuestionModel question) {

        QuestionType questionType = question.getQuestionType();

        switch (questionType) {
            case SINGLE_CHOICE:
                QuestionSingleChoice questionSC = new QuestionSingleChoice();
                questionSC.setId(question.getId());
                questionSC.setText(question.getText());
                questionSC.setType(QuestionType.SINGLE_CHOICE);
                questionSC.setChoices(question.getSingleChoiceAnswers());
                questionSC.setExpectedValue(question.getSingleChoiceExpectedAnswer());
                questionSC.setLabels(question.getLabels());
                questionService.saveQuestion(questionSC);
                break;
            case MULTIPLE_CHOICE:
                QuestionMultipleChoice questionMC = new QuestionMultipleChoice();
                questionMC.setId(question.getId());
                questionMC.setText(question.getText());
                questionMC.setType(QuestionType.MULTIPLE_CHOICE);
                questionMC.setChoices(question.getMultipleChoiceAnswers());
                questionMC.setExpectedValue(question.getMultipleChoiceExpectedAnswers());
                questionMC.setLabels(question.getLabels());
                questionService.saveQuestion(questionMC);
                break;

        }
        return "login";
    }

    @RequestMapping(value = "/questionpool")
    public ModelAndView loadAllQuestions() {
        ModelAndView modelAndView = new ModelAndView("question_list");

//        modelAndView.addObject("questions", QuestionUtils.convertToModel(questionService.getAll()));

        return modelAndView;
    }

    @PostMapping(value = "searchQuestions")
    public ModelAndView search(SearchFilter searchFilter) {

        List<Question> matchedQuestions = questionService.getQuestionByCriteria(searchFilter.getPhrase(), searchFilter.getLabels());


        ModelAndView modelAndView = new ModelAndView("question_list");
        modelAndView.addObject("questions", matchedQuestions);
        return modelAndView;
    }
    @RequestMapping(value =  "/loadquestions")
    public ModelAndView loadQuestions() {

        ModelAndView modelAndView = new ModelAndView("question_list");

        QuestionSingleChoice questionSingleChoice = new QuestionSingleChoice();
        questionSingleChoice.setId(1L);
        questionSingleChoice.setText("What is the difference between A and B?");
        questionSingleChoice.setType(QuestionType.SINGLE_CHOICE);
        questionSingleChoice.setChoices(Lists.newArrayList("no difference", "A is not B", "They are different letters"));
        questionSingleChoice.setExpectedValue("They are different letters");

        QuestionMultipleChoice questionMultipleChoice = new QuestionMultipleChoice();
        questionMultipleChoice.setId(2L);
        questionMultipleChoice.setText("Choose the answers that <br /> describe the situation the best if everything is dependent on the weather");
        questionMultipleChoice.setType(QuestionType.MULTIPLE_CHOICE);
        questionMultipleChoice.setChoices(Lists.newArrayList("it is sunny", "It is cloudy", "It is day", "It is night", "It is windy"));
        questionMultipleChoice.setExpectedValue(Lists.newArrayList("it is sunny", "It is windy"));


        QuestionSingleChoice questionSingleChoice2 = new QuestionSingleChoice();
        questionSingleChoice2.setId(3L);
        questionSingleChoice2.setText("Which statement is correct for the expression 2+2=4?");
        questionSingleChoice2.setType(QuestionType.SINGLE_CHOICE);
        questionSingleChoice2.setChoices(Lists.newArrayList("x is y", "2+2 is indeed 4", "sharks can speak"));
        questionSingleChoice2.setExpectedValue("2+2 is indeed 4");

        List<Question> loadedQuestions = Lists.newArrayList(questionSingleChoice, questionMultipleChoice, questionSingleChoice2);

        modelAndView.addObject("questions", loadedQuestions);
        return modelAndView;
    }
}
