package com.ee.intprep.controller;

import com.ee.intprep.QuestionModel;
import com.ee.intprep.entity.Label;
import com.ee.intprep.repositories.LabelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LabelController {

    @Autowired
    LabelRepository labelRepository;

    @RequestMapping(value = "/gotocreatelabel")
    public ModelAndView goToCreateLabel() {
        ModelAndView modelAndView = new ModelAndView("label_create");

        Label label = new Label();
        modelAndView.addObject("label", label);

        return modelAndView;
    }

    @PostMapping(value = "/createlabel")
    public String createLabel(Label label) {

        //TODO: implement service and check that same lebel cannot be saved twice;
        labelRepository.save(label);

        return "label_create";
    }
}
