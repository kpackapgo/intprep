package com.ee.intprep.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class QuestionBundleController {

    @RequestMapping("/home")
    public String home() {
        return "home";
    }
}
