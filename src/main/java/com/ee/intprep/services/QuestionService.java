package com.ee.intprep.services;

import com.ee.intprep.entity.Label;
import com.ee.intprep.entity.question.Question;
import com.ee.intprep.repositories.QuestionRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionService {

    private QuestionRepository questionRepository;

    @Autowired
    public QuestionService(QuestionRepository questionRepository) {
        this.questionRepository = questionRepository;
    }

    public void saveQuestion(Question question) {
        questionRepository.save(question);
    }

    public List<Question> getAll() {
        return Lists.newArrayList(questionRepository.findAll());
    }

    public List<Question> getQuestionByCriteria(String phrase, List<Label> labels) {

        List<Question> matchedQuestions = Lists.newArrayList();
        if(phrase.trim().equals("")) {
            if (labels.size() != 0) {
                matchedQuestions = questionRepository.findByLabelsIn(labels);
            } else {
               matchedQuestions = questionRepository.findAll();
            }
        } else {
            if (labels.size() != 0) {
                matchedQuestions = questionRepository.findByTextContainingAndLabelsIn(phrase, labels);
            } else {
                matchedQuestions = questionRepository.findByTextContaining(phrase);
            }
        }

        return matchedQuestions;
    }
}
