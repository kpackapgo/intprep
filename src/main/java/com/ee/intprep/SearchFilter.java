package com.ee.intprep;

import com.ee.intprep.entity.Label;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class SearchFilter {

    String phrase ;
    List<Label> labels;
}
